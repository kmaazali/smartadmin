<!DOCTYPE html>
<html lang="en-us">
	<?php require_once('includes/head.php'); ?>

	<body class="">
		
		<!-- HEADER -->
		<?php require_once('includes/header.php'); ?>
		<!-- END HEADER -->
		
		<!-- ASIDE -->
		<?php require_once('includes/aside.php'); ?>
		<!-- END ASIDE -->

		<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">

				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>

				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li>Home</li><li>E-commerce</li><li>Products View</li>
				</ol>

			</div>
			<!-- END RIBBON -->
			
			

			<!-- MAIN CONTENT -->
			<div id="content">

				<!-- row -->
				<div class="row">
					
					<!-- col -->
					<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
						<h1 class="page-title txt-color-blueDark">
							
							<!-- PAGE HEADER -->
							<i class="fa-fw fa fa-home"></i> 
								E-commerce
							<span>>  
								Products View
							</span>
						</h1>
					</div>
					<!-- end col -->
					
					<!-- right side of the page with the sparkline graphs -->
					<!-- col -->
					<div class="col-xs-12 col-sm-5 col-md-5 col-lg-8 text-right">
						
						<a href="javascript:void(0);" class="btn btn-default shop-btn">
							<i class="fa fa-3x fa-shopping-cart"></i>
							<span class="air air-top-right label-danger txt-color-white padding-5">10</span>
						</a>
						<a href="javascript:void(0);" class="btn btn-default">
							<i class="fa fa-3x fa-print"></i>
						</a>
						
					</div>
					<!-- end col -->
					
				</div>
				<!-- end row -->

				<!--
					The ID "widget-grid" will start to initialize all widgets below 
					You do not need to use widgets if you dont want to. Simply remove 
					the <section></section> and you can use wells or panels instead 
					-->

				<!-- widget grid -->
				<section id="widget-grid" class="">

					<!-- row -->

					<div class="row">

						<div class="col-sm-6 col-md-6 col-lg-6">
							<!-- product -->
							<div class="product-content product-wrap clearfix">
								<div class="row">
										<div class="col-md-5 col-sm-12 col-xs-12">
											<div class="product-image"> 
												<img src="img/demo/e-comm/1.png" alt="194x228" class="img-responsive"> 
												<span class="tag2 hot">
													HOT
												</span> 
											</div>
										</div>
										<div class="col-md-7 col-sm-12 col-xs-12">
										<div class="product-deatil">
												<h5 class="name">
													<a href="products-detail.php">
														Product Name Title Here <span>Category</span>
													</a>
												</h5>
												<p class="price-container">
													<span>$99</span>
												</p>
												<span class="tag1"></span> 
										</div>
										<div class="description">
											<p>Proin in ullamcorper lorem. Maecenas eu ipsum </p>
										</div>
										<div class="product-info smart-form">
											<div class="row">
												<div class="col-md-6 col-sm-6 col-xs-6"> 
													<a href="javascript:void(0);" class="btn btn-success">Add to cart</a>
												</div>
												<div class="col-md-6 col-sm-6 col-xs-6">
													<div class="rating">
														<input type="radio" name="stars-rating" id="stars-rating-5">
														<label for="stars-rating-5"><i class="fa fa-star"></i></label>
														<input type="radio" name="stars-rating" id="stars-rating-4">
														<label for="stars-rating-4"><i class="fa fa-star"></i></label>
														<input type="radio" name="stars-rating" id="stars-rating-3">
														<label for="stars-rating-3"><i class="fa fa-star text-primary"></i></label>
														<input type="radio" name="stars-rating" id="stars-rating-2">
														<label for="stars-rating-2"><i class="fa fa-star text-primary"></i></label>
														<input type="radio" name="stars-rating" id="stars-rating-1">
														<label for="stars-rating-1"><i class="fa fa-star text-primary"></i></label>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- end product -->
						</div>	

						<div class="col-sm-6 col-md-6 col-lg-6">
							<!-- product -->
							<div class="product-content product-wrap clearfix">
								<div class="row">
										<div class="col-md-5 col-sm-12 col-xs-12">
											<div class="product-image"> 
												<img src="img/demo/e-comm/2.png" alt="194x228" class="img-responsive"> 
												<span class="tag2 sale">
													Sale
												</span> 
											</div>
										</div>
										<div class="col-md-7 col-sm-12 col-xs-12">
										<div class="product-deatil">
												<h5 class="name">
													<a href="#">
														Product Name Title Here <span>Category</span>
													</a>
												</h5>
												<p class="price-container">
													<span>$109</span>
												</p>
												<span class="tag1"></span> 
										</div>
										<div class="description">
												<p>Proin in ullamcorper lorem. Maecenas eu ipsum </p>
										</div>
										<div class="product-info smart-form">
											<div class="row">
												<div class="col-md-6 col-sm-6 col-xs-6"> <a href="javascript:void(0);" class="btn btn-success">Add to cart</a> </div>
												<div class="col-md-6 col-sm-6 col-xs-6">
													<div class="rating">

														<input type="radio" name="stars-rating" id="stars-rating-5">
														<label for="stars-rating-5"><i class="fa fa-star"></i></label>
														<input type="radio" name="stars-rating" id="stars-rating-4">
														<label for="stars-rating-4"><i class="fa fa-star"></i></label>
														<input type="radio" name="stars-rating" id="stars-rating-3">
														<label for="stars-rating-3"><i class="fa fa-star text-primary"></i></label>
														<input type="radio" name="stars-rating" id="stars-rating-2">
														<label for="stars-rating-2"><i class="fa fa-star text-primary"></i></label>
														<input type="radio" name="stars-rating" id="stars-rating-1">
														<label for="stars-rating-1"><i class="fa fa-star text-primary"></i></label>

													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- end product -->
						</div>	

						<div class="col-sm-6 col-md-6 col-lg-4">
							<!-- product -->
							<div class="product-content product-wrap clearfix">
								<div class="row">
										<div class="col-md-5 col-sm-12 col-xs-12">
											<div class="product-image"> 
												<img src="img/demo/e-comm/3.png" alt="194x228" class="img-responsive"> 
												<span class="tag2 hot">
													HOT
												</span> 
											</div>
										</div>
										<div class="col-md-7 col-sm-12 col-xs-12">
										<div class="product-deatil">
												<h5 class="name">
													<a href="#">
														Product Name Title Here <span>Category</span>
													</a>
												</h5>
												<p class="price-container">
													<span>$399</span>
												</p>
												<span class="tag1"></span> 
										</div>
										<div class="description">
												<p>Proin in ullamcorper lorem. Maecenas eu ipsum </p>
										</div>
									</div>
								</div>
							</div>
							<!-- end product -->
						</div>			

						<div class="col-sm-6 col-md-6 col-lg-4">
							<!-- product -->
							<div class="product-content product-wrap clearfix">
								<div class="row">
										<div class="col-md-5 col-sm-12 col-xs-12">
											<div class="product-image"> 
												<img src="img/demo/e-comm/4.png" alt="194x228" class="img-responsive"> 
												<span class="tag2 sale">
													Sale
												</span> 
											</div>
										</div>
										<div class="col-md-7 col-sm-12 col-xs-12">
										<div class="product-deatil">
												<h5 class="name">
													<a href="#">
														Product Name Title Here <span>Category</span>
													</a>
												</h5>
												<p class="price-container">
													<span>$19</span>
												</p>
												<span class="tag1"></span> 
										</div>
										<div class="description">
											<p>Proin in ullamcorper lorem. Maecenas eu ipsum </p>
										</div>
									</div>
								</div>
							</div>
							<!-- end product -->
						</div>	

						<div class="col-sm-6 col-md-6 col-lg-4">
							<!-- product -->
							<div class="product-content product-wrap clearfix">
								<div class="row">
										<div class="col-md-5 col-sm-12 col-xs-12">
											<div class="product-image"> 
												<img src="img/demo/e-comm/5.png" alt="194x228" class="img-responsive"> 
												<span class="tag2 sale">
													Sale
												</span> 
											</div>
										</div>
										<div class="col-md-7 col-sm-12 col-xs-12">
										<div class="product-deatil">
												<h5 class="name">
													<a href="#">
														Product Name Title Here <span>Category</span>
													</a>
												</h5>
												<p class="price-container">
													<span>$195</span>
												</p>
												<span class="tag1"></span> 
										</div>
										<div class="description">
												<p>Proin in ullamcorper lorem. Maecenas eu ipsum </p>
										</div>
									</div>
								</div>
							</div>
							<!-- end product -->
						</div>	

						<div class="col-sm-6 col-md-6 col-lg-4">
							<!-- product -->
							<div class="product-content product-wrap clearfix">
								<div class="row">
										<div class="col-md-5 col-sm-12 col-xs-12">
											<div class="product-image"> 
												<img src="img/demo/e-comm/7.png" alt="194x228" class="img-responsive"> 
												<span class="tag2 sale">
													Sale
												</span> 
											</div>
										</div>
										<div class="col-md-7 col-sm-12 col-xs-12">
										<div class="product-deatil">
												<h5 class="name">
													<a href="#">
														Product Name Title Here <span>Category</span>
													</a>
												</h5>
												<p class="price-container">
													<span>$99</span>
												</p>
												<span class="tag1"></span> 
										</div>
										<div class="description">
											<p>Proin in ullamcorper lorem. Maecenas eu ipsum </p>
										</div>
									</div>
								</div>
							</div>
							<!-- end product -->
						</div>	

						<div class="col-sm-6 col-md-6 col-lg-4">
							<!-- product -->
							<div class="product-content product-wrap clearfix">
								<div class="row">
										<div class="col-md-5 col-sm-12 col-xs-12">
											<div class="product-image"> 
												<img src="img/demo/e-comm/8.png" alt="194x228" class="img-responsive"> 
												<span class="tag2 sale">
													Sale
												</span> 
											</div>
										</div>
										<div class="col-md-7 col-sm-12 col-xs-12">
										<div class="product-deatil">
												<h5 class="name">
													<a href="#">
														Product Name Title Here <span>Category</span>
													</a>
												</h5>
												<p class="price-container">
													<span>$109</span>
												</p>
												<span class="tag1"></span> 
										</div>
										<div class="description">
												<p>Proin in ullamcorper lorem. Maecenas eu ipsum </p>
										</div>
									</div>
								</div>
							</div>
							<!-- end product -->
						</div>	

						<div class="col-sm-6 col-md-6 col-lg-4">
							<!-- product -->
							<div class="product-content product-wrap clearfix">
								<div class="row">
										<div class="col-md-5 col-sm-12 col-xs-12">
											<div class="product-image"> 
												<img src="img/demo/e-comm/9.png" alt="194x228" class="img-responsive"> 
												<span class="tag2 sale">
													Sale
												</span> 
											</div>
										</div>
										<div class="col-md-7 col-sm-12 col-xs-12">
										<div class="product-deatil">
												<h5 class="name">
													<a href="#">
														Product Name Title Here <span>Category</span>
													</a>
												</h5>
												<p class="price-container">
													<span>$399</span>
												</p>
												<span class="tag1"></span> 
										</div>
										<div class="description">
												<p>Proin in ullamcorper lorem. Maecenas eu ipsum</p>
										</div>
									</div>
								</div>
							</div>
							<!-- end product -->
						</div>
						
						<div class="col-sm-12 text-center">
							<a href="javascript:void(0);" class="btn btn-primary btn-lg">Load more <i class="fa fa-arrow-down"></i></a>
						</div>

					</div>

					<!-- end row -->

				</section>
				<!-- end widget grid -->

			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->

		<!-- PAGE FOOTER -->
		<?php require_once('includes/footer.php'); ?>
		<!-- END PAGE FOOTER -->

		<!-- SHORTCUT AREA : With large tiles (activated via clicking user name tag)
		Note: These tiles are completely responsive,
		you can add as many as you like
		-->
		<div id="shortcut">
			<ul>
				<li>
					<a href="inbox.html" class="jarvismetro-tile big-cubes bg-color-blue"> <span class="iconbox"> <i class="fa fa-envelope fa-4x"></i> <span>Mail <span class="label pull-right bg-color-darken">14</span></span> </span> </a>
				</li>
				<li>
					<a href="calendar.html" class="jarvismetro-tile big-cubes bg-color-orangeDark"> <span class="iconbox"> <i class="fa fa-calendar fa-4x"></i> <span>Calendar</span> </span> </a>
				</li>
				<li>
					<a href="gmap-xml.html" class="jarvismetro-tile big-cubes bg-color-purple"> <span class="iconbox"> <i class="fa fa-map-marker fa-4x"></i> <span>Maps</span> </span> </a>
				</li>
				<li>
					<a href="invoice.html" class="jarvismetro-tile big-cubes bg-color-blueDark"> <span class="iconbox"> <i class="fa fa-book fa-4x"></i> <span>Invoice <span class="label pull-right bg-color-darken">99</span></span> </span> </a>
				</li>
				<li>
					<a href="gallery.html" class="jarvismetro-tile big-cubes bg-color-greenLight"> <span class="iconbox"> <i class="fa fa-picture-o fa-4x"></i> <span>Gallery </span> </span> </a>
				</li>
				<li>
					<a href="profile.html" class="jarvismetro-tile big-cubes selected bg-color-pinkDark"> <span class="iconbox"> <i class="fa fa-user fa-4x"></i> <span>My Profile </span> </span> </a>
				</li>
			</ul>
		</div>
		<!-- END SHORTCUT AREA -->

		<!--================================================== -->

		<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
		<script data-pace-options='{ "restartOnRequestAfter": true }' src="js/plugin/pace/pace.min.js"></script>

		<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script>
			if (!window.jQuery) {
				document.write('<script src="js/libs/jquery-3.2.1.min.js"><\/script>');
			}
		</script>

		<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="js/libs/jquery-ui.min.js"><\/script>');
			}
		</script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="js/app.config.js"></script>

		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 

		<!-- BOOTSTRAP JS -->
		<script src="js/bootstrap/bootstrap.min.js"></script>

		<!-- CUSTOM NOTIFICATION -->
		<script src="js/notification/SmartNotification.min.js"></script>

		<!-- JARVIS WIDGETS -->
		<script src="js/smartwidgets/jarvis.widget.min.js"></script>

		<!-- EASY PIE CHARTS -->
		<script src="js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

		<!-- SPARKLINES -->
		<script src="js/plugin/sparkline/jquery.sparkline.min.js"></script>

		<!-- JQUERY VALIDATE -->
		<script src="js/plugin/jquery-validate/jquery.validate.min.js"></script>

		<!-- JQUERY MASKED INPUT -->
		<script src="js/plugin/masked-input/jquery.maskedinput.min.js"></script>

		<!-- JQUERY SELECT2 INPUT -->
		<script src="js/plugin/select2/select2.min.js"></script>

		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

		<!-- browser msie issue fix -->
		<script src="js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

		<!-- FastClick: For mobile devices -->
		<script src="js/plugin/fastclick/fastclick.min.js"></script>

		<!--[if IE 8]>

		<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

		<![endif]-->

		<!-- Demo purpose only -->
		<script src="js/demo.min.js"></script>

		<!-- MAIN APP JS FILE -->
		<script src="js/app.min.js"></script>

		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="js/speech/voicecommand.min.js"></script>

		<!-- SmartChat UI : plugin -->
		<script src="js/smart-chat-ui/smart.chat.ui.min.js"></script>
		<script src="js/smart-chat-ui/smart.chat.manager.min.js"></script>

		<!-- PAGE RELATED PLUGIN(S) 
		<script src="..."></script>-->

		<script>

			$(document).ready(function() {
			 	
				/* DO NOT REMOVE : GLOBAL FUNCTIONS!
				 *
				 * pageSetUp(); WILL CALL THE FOLLOWING FUNCTIONS
				 *
				 * // activate tooltips
				 * $("[rel=tooltip]").tooltip();
				 *
				 * // activate popovers
				 * $("[rel=popover]").popover();
				 *
				 * // activate popovers with hover states
				 * $("[rel=popover-hover]").popover({ trigger: "hover" });
				 *
				 * // activate inline charts
				 * runAllCharts();
				 *
				 * // setup widgets
				 * setup_widgets_desktop();
				 *
				 * // run form elements
				 * runAllForms();
				 *
				 ********************************
				 *
				 * pageSetUp() is needed whenever you load a page.
				 * It initializes and checks for all basic elements of the page
				 * and makes rendering easier.
				 *
				 */
				
				 pageSetUp();
				 
				/*
				 * ALL PAGE RELATED SCRIPTS CAN GO BELOW HERE
				 * eg alert("my home function");
				 * 
				 * var pagefunction = function() {
				 *   ...
				 * }
				 * loadScript("js/plugin/_PLUGIN_NAME_.js", pagefunction);
				 * 
				 * TO LOAD A SCRIPT:
				 * var pagefunction = function (){ 
				 *  loadScript(".../plugin.js", run_after_loaded);	
				 * }
				 * 
				 * OR
				 * 
				 * loadScript(".../plugin.js", run_after_loaded);
				 */
				
			})
		
		</script>

		<!-- Your GOOGLE ANALYTICS CODE Below -->
		<script>
			var _gaq = _gaq || [];
				_gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
				_gaq.push(['_trackPageview']);
			
			(function() {
				var ga = document.createElement('script');
				ga.type = 'text/javascript';
				ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(ga, s);
			})();

		</script>

	</body>

</html>