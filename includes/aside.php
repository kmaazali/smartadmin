<aside id="left-panel">

			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as it --> 
					
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
						<img src="img/avatars/sunny.png" alt="me" class="online" /> 
						<span>
							Aleem Zada
						</span>
						<i class="fa fa-angle-down"></i>
					</a> 
					
				</span>
			</div>
			<!-- end user info -->

			<!-- NAVIGATION : This navigation is also responsive-->
			<nav>
				<ul>
					<!-- Users -->
					<li>
						<a href="#"><i class="fa fa-lg fa-fw fa-table"></i> <span class="menu-item-parent">Users</span></a>
						<ul>
							<li>
								<a href="users.php">All Users</a>
							</li>
						</ul>
					</li>

					<!-- Shop -->
					<li>
						<a href="#"><i class="fa fa-lg fa-fw fa-table"></i> <span class="menu-item-parent">Shop</span></a>
						<ul>
							<li>
								<a href="shops.php">All Shops</a>
							</li>
							<li>
								<a href="add_shop.php">Add Shop</a>
							</li>
						</ul>
					</li>

					<!-- Products -->
					<li>
						<a href="#"><i class="fa fa-lg fa-fw fa-table"></i> <span class="menu-item-parent">Product</span></a>
						<ul>
							<li>
								<a href="products.php">All Products</a>
							</li>
							<li>
								<a href="add_product.php">Add Product</a>
							</li>
						</ul>
					</li>
					<!-- e-commerce -->
					<li>
						<a href="#"><i class="fa fa-lg fa-fw fa-shopping-cart"></i> <span class="menu-item-parent">E-Commerce</span></a>
						<ul>
							<li><a href="orders.php">Orders</a></li>
							<li><a href="products-view.php">Products View</a></li>
							<li><a href="products-detail.php">Products Detail</a></li>
						</ul>
					</li>	
				</ul>
			</nav>
			
			<span class="minifyme" data-action="minifyMenu"> 
				<i class="fa fa-arrow-circle-left hit"></i> 
			</span>

		</aside>